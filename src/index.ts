// Import our Css
import "./tailwind.css";

// Import Vue
import Vue from "vue";

// Import Libraries
import VueCompositionApi from "@vue/composition-api";
Vue.use(VueCompositionApi);

// Import our App
import App from "./App.vue";

// Set up Vue with our app
new Vue({
  el: "#app",
  render: h => h(App)
});
