import { mount } from "@vue/test-utils";
import Main from "./Main.vue";

describe("Main.vue", () => {
  test("setup", () => {
    const wrapper = mount(Main);
    expect(wrapper.text()).toContain("Tim's Vue Starter Template");
  });
});
