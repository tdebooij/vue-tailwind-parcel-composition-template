# Vue-Tailwind-Parcel-Composition-Template

Starter template for frontend development using Vue 2 (with the new composition API in lieu of Vue 3), TypeScript, Tailwind for styling and Parcel for bundling.

# Getting started

Prerequisites

- NodeJs

Clone this repository

```bash
git clone git+https://gitlab.com/tdebooij/vue-tailwind-parcel-composition-template.git
```

Install modules

```bash
npm install
```

Run in development mode with HRM

```bash
npm run start
```

Build for production

```bash
npm run build
```

# To do:

- [x] Parcel
- [x] Vue
- [x] Tailwind
- [x] TypeScript
- [x] Composition API
- [ ] Unit tests
- [ ] Gitlab Pages
